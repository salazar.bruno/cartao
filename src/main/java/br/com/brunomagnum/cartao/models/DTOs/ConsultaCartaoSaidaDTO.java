package br.com.brunomagnum.cartao.models.DTOs;

public class ConsultaCartaoSaidaDTO {

    private int id;
    private String numero;
    private int clienteId;


    public ConsultaCartaoSaidaDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

}
