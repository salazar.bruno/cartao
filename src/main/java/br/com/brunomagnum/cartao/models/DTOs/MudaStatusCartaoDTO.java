package br.com.brunomagnum.cartao.models.DTOs;

public class MudaStatusCartaoDTO {

    private Boolean ativo;

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

}
