package br.com.brunomagnum.cartao.controllers;

import br.com.brunomagnum.cartao.models.Cartao;
import br.com.brunomagnum.cartao.models.DTOs.CartaoEntradaDTO;
import br.com.brunomagnum.cartao.models.DTOs.CartaoSaidaDTO;
import br.com.brunomagnum.cartao.models.DTOs.ConsultaCartaoSaidaDTO;
import br.com.brunomagnum.cartao.models.DTOs.MudaStatusCartaoDTO;
import br.com.brunomagnum.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;


@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @GetMapping
    public Iterable<Cartao> buscaTodosCartoes(){
        return cartaoService.buscaTodosCartoes();
    }

    @GetMapping("/id/{id}")
    public Cartao buscaCartaoPorId(@PathVariable(name="id") Integer id){
        try{
            return cartaoService.buscaCartaoPorId(id);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{numero}")
    public ConsultaCartaoSaidaDTO buscaCartaoPorNumero(@PathVariable(name="numero") String numero){
        try{
            Cartao cartao = cartaoService.buscaCartaoPorNumero(numero);

            ConsultaCartaoSaidaDTO cartaoSaida = new ConsultaCartaoSaidaDTO();
            cartaoSaida.setId(cartao.getId());
            cartaoSaida.setNumero(cartao.getNumero());
            cartaoSaida.setClienteId(cartao.getClienteId());

            return cartaoSaida;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PostMapping
    public ResponseEntity<CartaoSaidaDTO> criaCartao(@RequestBody @Valid CartaoEntradaDTO cartaoEntradaDTO){
        try{
            return ResponseEntity.status(201).body(cartaoService.criaCartao(cartaoEntradaDTO));
        } catch(RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,exception.getMessage());
        }
    }

    @PatchMapping("/{numero}")
    public ResponseEntity<CartaoSaidaDTO> mudaStatusCartao(@PathVariable(name="numero") String numero, @RequestBody MudaStatusCartaoDTO ativo){
        try{
            return ResponseEntity.status(200).body(cartaoService.mudaStatusCartao(numero, ativo));
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


}
