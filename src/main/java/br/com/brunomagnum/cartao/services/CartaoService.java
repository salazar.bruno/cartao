package br.com.brunomagnum.cartao.services;

import br.com.brunomagnum.cartao.clients.ClienteClient;
import br.com.brunomagnum.cartao.models.Cartao;
import br.com.brunomagnum.cartao.models.Cliente;
import br.com.brunomagnum.cartao.models.DTOs.CartaoEntradaDTO;
import br.com.brunomagnum.cartao.models.DTOs.CartaoSaidaDTO;
import br.com.brunomagnum.cartao.models.DTOs.MudaStatusCartaoDTO;
import br.com.brunomagnum.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public CartaoSaidaDTO criaCartao(CartaoEntradaDTO cartaoEntradaDTO){
        Cliente cliente = clienteClient.buscaClientePorId(cartaoEntradaDTO.getClienteId());

        if (cliente != null){

            Cartao cartao = new Cartao();
            cartao.setNumero(cartaoEntradaDTO.getNumero());
            cartao.setClienteId(cliente.getId());
            cartao.setAtivo(false);
            cartaoRepository.save(cartao);

            CartaoSaidaDTO cartaoSaidaDTO = new CartaoSaidaDTO();
            cartaoSaidaDTO.setNumero(cartao.getNumero());
            cartaoSaidaDTO.setAtivo(cartao.isAtivo());
            cartaoSaidaDTO.setClienteId(cartao.getClienteId());
            cartaoSaidaDTO.setId(cartao.getId());
            return cartaoSaidaDTO;
        }
        else {
            throw new RuntimeException("Cliente não ecziste");
        }
    }

    public Iterable<Cartao> buscaTodosCartoes(){
        return cartaoRepository.findAll();
    }

    public Cartao buscaCartaoPorId(Integer id){
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);

        if (cartaoOptional.isPresent()){
            return cartaoOptional.get();
        } else{
            throw new RuntimeException("ID de Cartao nao encontrado");
        }
    }

    public Cartao buscaCartaoPorNumero(String numero){
        Optional<Cartao> cartaoOptional = cartaoRepository.findByNumero(numero);

        if (cartaoOptional.isPresent()){
            return cartaoOptional.get();
        } else{
            throw new RuntimeException("ID de Cartao nao encontrado");
        }
    }

    public CartaoSaidaDTO mudaStatusCartao(String numero, MudaStatusCartaoDTO ativo) {
        Cartao cartao = buscaCartaoPorNumero(numero);

        cartao.setAtivo(ativo.getAtivo());
        cartaoRepository.save(cartao);

        CartaoSaidaDTO cartaoSaidaDTO = new CartaoSaidaDTO();
        cartaoSaidaDTO.setId(cartao.getId());
        cartaoSaidaDTO.setClienteId(cartao.getClienteId());
        cartaoSaidaDTO.setNumero(cartao.getNumero());
        cartaoSaidaDTO.setAtivo(cartao.isAtivo());

        return cartaoSaidaDTO;
    }

}
