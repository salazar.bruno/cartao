package br.com.brunomagnum.cartao.clients;

import br.com.brunomagnum.cartao.models.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name= "Cliente")
public interface ClienteClient {

    @GetMapping("/cliente/{id}")
    Cliente buscaClientePorId(@PathVariable Integer id);
}
